{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}

module Types where

import Data.Aeson
import Data.Map
import GHC.Generics
import Data.UUID

data User = User { userUuid :: String, username :: String } deriving (Generic, Show, Eq, FromJSON, ToJSON)
data Game = WaitingGame { gameUuid :: UUID, gameName :: String, firstPlayer :: User }
          | StartedGame { gameUuid :: UUID, gameName :: String, players :: (User, User), board :: Board, step :: Int, removedStones :: (Int, Int), winnerStatus :: WinnerStatus }
          deriving (Generic, Show, Eq, FromJSON, ToJSON)
data WinnerStatus = GameInProgress
                  | Standoff
                  | Winner { winner :: User }
                  deriving (Generic, Show, Eq, FromJSON, ToJSON)
data Board = Board { intersections :: Map Coordinate (Maybe Stone) } deriving (Generic, Show, Eq, FromJSON, ToJSON)
data Coordinate = Coordinate { x :: Int, y :: Int } deriving (Generic, Ord, Show, Eq, FromJSON, ToJSON, FromJSONKey, ToJSONKey)
data Stone = BlackStone | WhiteStone deriving (Generic, Show, Eq, FromJSON, ToJSON)

data CreateGameRequestParams = CreateGameRequestParams  { user_info :: User, game_name :: String } deriving (Generic, Show, Eq, FromJSON, ToJSON)
data StartGameRequestParams = StartGameRequestParams { user_data :: User, game_uuid :: UUID } deriving (Generic, Show, Eq, FromJSON, ToJSON)
data MoveRequestDto = MoveRequestDto { moveRequestGameUuid :: UUID, moveRequestCoordinate :: Coordinate } deriving (Generic, Show, Eq, FromJSON, ToJSON)
data ErrorMsg = ErrorMsg { msg :: String } deriving (Generic, Show, Eq, FromJSON, ToJSON)

data State = State [Game]  deriving (Show)
