module Logic.GameStateLogic where

import Types
import qualified Data.Map (insert, empty)
import Data.UUID
import Data.List (find, delete, filter)

boardSize :: Int
boardSize = 19

createBoard :: Board
createBoard = Board (helper Data.Map.empty boardSize boardSize)
    where helper board x y | x > 0 && y > 1 = helper (Data.Map.insert (Coordinate x y) Nothing board) x (y - 1)
                           | x > 0 && y == 1 = helper (Data.Map.insert (Coordinate x y) Nothing board) (x - 1) boardSize
                           | otherwise = board

createStartedGame :: Game -> User -> Game
createStartedGame (WaitingGame uuid name firstPlayer) user = StartedGame { gameUuid=uuid, 
                                                                           gameName=name, 
                                                                           players=(firstPlayer, user), 
                                                                           board=createBoard, 
                                                                           step=1,
                                                                           removedStones=(0, 0),
                                                                           winnerStatus=GameInProgress }
createStartedGame startedGame user = startedGame

findGameByUuid :: UUID -> State -> Maybe Game
findGameByUuid uuid (State games) = find (\game -> (gameUuid game) == uuid) games

{-
  Remove *replaceableGame* from State and add *newGame* to State
-}
replaceGameInState :: Game -> Game -> State -> State
replaceGameInState replaceableGame newGame (State games) = State (newGame:(delete replaceableGame games))

isWaitingGame :: Game -> Bool
isWaitingGame WaitingGame {} = True
isWaitingGame _ = False

{-
  Gets only waiting games for list in main page of the app
-}
getGamesFromState :: State -> [Game]
getGamesFromState (State games) = filter isWaitingGame games
