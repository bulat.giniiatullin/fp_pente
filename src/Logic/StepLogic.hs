module Logic.StepLogic where

import Types
import qualified Data.Map (insert, filter, (!), lookup, null)
import Data.Maybe (isNothing)
import Control.Monad (replicateM, foldM)

{-
  This function is entry point for every move. Secuentially checks all conditions and rules, makes necessary changes on the board. All called functions are defined below.
  If any of called functions returns "Left String", stops remaining executions and immediately returns "Left String" with error message without changing state of the board.
-}
doMove :: Game -> Coordinate -> Either String Game
doMove game coord = foldM (flip id) game $ map (\func -> func coord) [checkTournamentRule, checkStep, checkIfRemovableAndRemove, checkEnd, makeStep, incrementStep]

{-
  Checks if intersection is already used with another stone.
-}
checkStep :: Coordinate -> Game -> Either String Game
checkStep coord@(Coordinate x y) game@(StartedGame{board=(Board intersections), step=step}) = 
    case Data.Map.lookup coord intersections of
        Just val -> case val of 
            Nothing -> Right game
            Just _ -> Left "The intersection is used"
        Nothing -> Left "This intersection not exists"

{-
  Checks and removes (if needed) every possible combination (horizontal, vertical and diagonal).
-}
checkIfRemovableAndRemove :: Coordinate -> Game -> Either String Game
checkIfRemovableAndRemove coord game = Right $ foldr (checkIfRemovableBetweenAndRemove coord . listToPair) game (tail $ replicateM 2 [0, 3, -3])

listToPair :: [Int] -> (Int, Int)
listToPair (el1:el2:_) = (el1, el2)

{-
  Checks and removes (if needed) specific combination.
-}
checkIfRemovableBetweenAndRemove :: Coordinate ->  (Int, Int) -> Game -> Game
checkIfRemovableBetweenAndRemove coord@(Coordinate x y) (shiftX, shiftY) game@(StartedGame{board=(Board intersections), step=step}) = 
    case Data.Map.lookup (Coordinate (x+shiftX) (y+shiftY)) intersections of 
    Nothing  -> game
    Just val -> case val of 
        Nothing -> game
        Just val -> 
                if val == currentColor game && checkIfOppositeBetween (getPrevNumber shiftX) (getPrevNumber shiftY) coord game then
                    removeOppositeBetween (getPrevNumber shiftX) (getPrevNumber shiftY) coord game
                else
                    game

checkIfOppositeBetween :: Int -> Int -> Coordinate -> Game -> Bool
checkIfOppositeBetween shiftX shiftY coord@(Coordinate x y) game@(StartedGame{board=(Board intersections)})
    | shiftX == 0 && shiftY == 0 = True
    | otherwise = 
        (intersections Data.Map.! (Coordinate (x+shiftX) (y+shiftY))) == Just (oppositeColor game) &&
        checkIfOppositeBetween (getPrevNumber shiftX) (getPrevNumber shiftY) coord game

removeOppositeBetween :: Int -> Int -> Coordinate -> Game -> Game
removeOppositeBetween shiftX shiftY coord@(Coordinate x y) game@(StartedGame{board=(Board intersections)})
    | shiftX == 0 && shiftY == 0 = game
    | otherwise = removeOppositeBetween (getPrevNumber shiftX) (getPrevNumber shiftY) coord (game {removedStones=(incrementRemovedStones game), board=(Board (Data.Map.insert (Coordinate (x+shiftX) (y+shiftY)) Nothing intersections))})

incrementRemovedStones :: Game -> (Int, Int)
incrementRemovedStones game@(StartedGame {step=step, removedStones=(r1, r2)}) = 
    if step `mod` 2 == 0 then
        (r1+1, r2)
    else
        (r1, r2+1)
    
getPrevNumber :: Int -> Int
getPrevNumber n = signum n * (abs n - 1)

checkEnd :: Coordinate -> Game -> Either String Game
checkEnd coord@(Coordinate x y) game@(StartedGame{board=(Board intersections), removedStones=(r1, r2), players=(user1, user2)}) = 
    if r1 >= 10 then 
        Right game {winnerStatus=(Winner user2)}
    else if r2 >= 10 then
        Right game {winnerStatus=(Winner user1)}
    else if Data.Map.null $ Data.Map.filter isNothing intersections then
        Right game {winnerStatus=Standoff}
    else
        Right game

{-
  Inserts new stone into the board. Returns game with updated board.
-}
makeStep :: Coordinate -> Game -> Either String Game
makeStep coord@(Coordinate x y) game@(StartedGame{board=(Board intersections)}) = 
    Right (game {board=Board $ (Data.Map.insert (Coordinate x y) (Just $ currentColor game) intersections)})

incrementStep :: Coordinate -> Game -> Either String Game
incrementStep _ game@(StartedGame{step=step}) = Right $ game {step=step+1}

currentColor :: Game -> Stone
currentColor (StartedGame{step=step}) = if step `mod` 2 == 1 then WhiteStone else BlackStone

oppositeColor :: Game -> Stone
oppositeColor (StartedGame{step=step}) = if step `mod` 2 == 1 then BlackStone else WhiteStone

checkTournamentRule :: Coordinate -> Game -> Either String Game
checkTournamentRule (Coordinate x y) game@(StartedGame {step=step})
    | step == 1 = checkFirstStep
    | step == 3 = checkSecondStep
    | otherwise = Right game
    where
        checkFirstStep
            | x == 10 && y == 10 = Right game
            | otherwise          = Left "On White's first move stone must be placed at the center of the board"
        checkSecondStep
            | (x <= 7 || x >= 13) || (y <= 7 || y >= 13) = Right game
            | otherwise                                  = Left "On White's second move stone must be placed at 3 intersections farther from the center"
checkTournamentRule _ _ = Left "Not started game"
