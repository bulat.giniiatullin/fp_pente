{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module Api where

import Network.Wai (Middleware)
import Network.Wai.Middleware.Cors
import Data.Aeson
import Data.UUID
import Control.Concurrent.STM
import Servant
import Types
import ApiLogic

{-
  POST /create {"user_info": {User}, "game_name": ""} => {gameUuid: "", gameName: "", firstPlayer: {User}}
  GET /check/{gameUuid} => {Game} 
                           or {msg: "Error msg"} (if error)
  POST /start {"user_data": {User}, "game_uuid": ""} => 
    {gameUuid: "", gameName: "", players :: [{User}, {User}], board: {Board}, step: Int, removedStones: [Int, Int], winnerStatus: {WinnerStatus} } 
    or {msg: "Error msg"} (if error)
  GET /list => [{gameUuid: "", gameName: "", firstPlayer: {User}}]
  POST /move {"moveRequestGameUuid": "", "moveRequestCoordinate": {"x": Int, "y": Int}} => {StartedGame} 
                                                                                           or {msg: "Error msg"} (if error)
-}
type MyApi = "create" :> ReqBody '[JSON] CreateGameRequestParams :> Post '[JSON] Game
        :<|> "check" :> Capture "gameUuid" UUID :> Get '[JSON] Game
        :<|> "start" :> ReqBody '[JSON] StartGameRequestParams :> Post '[JSON] Game
        :<|> "list" :> Get '[JSON] [Game]
        :<|> "move" :> ReqBody '[JSON] MoveRequestDto :> Post '[JSON] Game

server :: TVar State -> Server MyApi
server stateVar = createGame stateVar
    :<|> checkGameStatus stateVar
    :<|> startGame stateVar
    :<|> listGames stateVar
    :<|> doMove stateVar

apiProxy :: Proxy MyApi
apiProxy = Proxy

allowCors :: Middleware
allowCors = cors (const $ Just appCorsResourcePolicy)

appCorsResourcePolicy :: CorsResourcePolicy
appCorsResourcePolicy = simpleCorsResourcePolicy {
    corsMethods = ["OPTIONS", "GET", "POST"],
    corsRequestHeaders = ["Authorization", "Content-Type", "Accept"]
    }

app :: TVar State -> Application
app var = allowCors $ serve apiProxy $ server var
