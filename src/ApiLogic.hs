{-# LANGUAGE OverloadedStrings #-}

module ApiLogic where

import Data.UUID
import Data.UUID.V4
import Data.ByteString.Lazy.UTF8 as LazyByteStringUTF8
import Data.Aeson
import Control.Monad.IO.Class
import Control.Concurrent.STM
import Servant
import Types
import Logic

createGame :: TVar State -> CreateGameRequestParams -> Handler Game
createGame state (CreateGameRequestParams user gameName) = do
    randUuid <- liftIO nextRandom
    let newWaitingGame = WaitingGame { gameUuid = randUuid, gameName = gameName, firstPlayer = user }
    liftIO $ atomically $ modifyTVar state (\(State gamesBefore) -> State (newWaitingGame:gamesBefore))
    return newWaitingGame

checkGameStatus :: TVar State -> UUID -> Handler Game
checkGameStatus state id = do
    currentState <- liftIO $ readTVarIO state
    let maybeFoundGame = findGameByUuid id currentState
    case maybeFoundGame of
         Nothing      -> throwError $ err404 { errBody = encode $ ErrorMsg "The game is not found" }
         (Just found) -> return found

startGame :: TVar State -> StartGameRequestParams -> Handler Game
startGame state (StartGameRequestParams user gameUuid) = do
    waitingGame <- checkGameStatus state gameUuid
    case waitingGame of
        (StartedGame {}) -> throwError $ err403 { errBody = encode $ ErrorMsg "The game is not started yet" }
        (WaitingGame {}) -> do
            let modifiedGame = createStartedGame waitingGame user
            liftIO $ atomically $ modifyTVar state (replaceGameInState waitingGame modifiedGame)
            return modifiedGame

listGames :: TVar State -> Handler [Game]
listGames state = do
    currentState <- liftIO $ readTVarIO state
    return $ getGamesFromState currentState

doMove :: TVar State -> MoveRequestDto -> Handler Game
doMove state (MoveRequestDto gameId coordinates) = do
    game <- checkGameStatus state gameId
    case game of
        (WaitingGame {}) -> throwError $ err403 { errBody = encode $ ErrorMsg "The game is not started yet" }
        (StartedGame {}) -> do
            let eitherUpdatedGame = Logic.doMove game coordinates
            case eitherUpdatedGame of
                (Right updatedGame) -> do
                    liftIO $ atomically $ modifyTVar state (replaceGameInState game updatedGame)
                    return updatedGame
                (Left errorMsg) -> throwError $ err403 { errBody = encode $ ErrorMsg $ errorMsg }
