import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import {NotificationContainer} from 'react-notifications';

import GamesListPage from "./components/GamesList/GamesListPage";
import GamePage from "./components/GamePage";
import CreateUser from "./components/CreateUser";
import CreateGame from "./components/CreateGame";

import './App.css';
import 'react-notifications/lib/notifications.css';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: undefined,
            game: undefined
        }
    }

    updateUser = (user) => {
        this.setState({user: user});
    };

    setCurrentGame = (game) => {
        this.setState({game: game});
    };

    render() {
        return (
            <div className={'App'}>
                <header className={'mainHeader'}>
                    Pente
                    <sup>alpha</sup>
                </header>
                <BrowserRouter basename="/pente">
                    <Switch>
                        <Route exact path='/' render={(props) => <CreateUser {...props} updateUser={this.updateUser}/>}/>
                        <Route exact path='/games' render={(props) => <GamesListPage {...props} user={this.state.user} setCurrentGame={this.setCurrentGame}/>}/>
                        <Route exact path='/create_game' render={(props) => <CreateGame {...props} user={this.state.user} setCurrentGame={this.setCurrentGame}/>}/>
                        <Route exact path='/play_game' render={(props) => <GamePage {...props} user={this.state.user} game={this.state.game}/>}/>
                    </Switch>
                </BrowserRouter>
                <NotificationContainer/>
            </div>
        );
    }
}
