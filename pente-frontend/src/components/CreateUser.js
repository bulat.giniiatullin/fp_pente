import React from "react";
import uuidv4 from "uuid/v4";

export default class CreateUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: ''
        }
    }

    handleChange = (event) => {
        this.setState({name: event.target.value});
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const user = {
            username: this.state.name,
            userUuid: uuidv4()
        };
        this.props.updateUser(user);
        console.log(`New user created: ${JSON.stringify(user)}`);
        this.props.history.push('/games');
    };

    render() {
        return (
            <div className={'formContainer'}>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Имя:
                        <input type="text" value={this.state.name} onChange={this.handleChange} />
                    </label>
                    <br/>
                    <input type="submit" value="Сохранить" />
                </form>
            </div>
        )
    }
}
