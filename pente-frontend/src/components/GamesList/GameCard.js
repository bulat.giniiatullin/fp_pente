import React from "react";

const GameCard = (props) => (
    <div className={'GameCard'}>
        <div>
            Game: {props.game.gameName}<br />
            <span style={{fontSize: '14px'}}>Created by: {props.game.firstPlayer.username}</span>
        </div>
        <div>
            <button onClick={() => props.joinGame(props.game.gameUuid)}>Присоединиться</button>
        </div>
    </div>
);

export default GameCard;
