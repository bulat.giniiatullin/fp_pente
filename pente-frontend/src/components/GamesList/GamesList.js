import React from "react";
import GameCard from "./GameCard";

const GamesList = (props) => (
    <div className={'GamesList'}>
        {props.games !== undefined && props.games.length > 0 ?
            props.games.map(game => <GameCard key={game.gameUuid} game={game} joinGame={props.joinGame}/>)
            : <p>Пока нет ни одной игры ( </p>
        }
    </div>
);

export default GamesList;
