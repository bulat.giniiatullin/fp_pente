import React from "react";
import Axios from "axios";
import GamesList from "./GamesList";
import {Link} from "react-router-dom";
import {catchAxiosErrors} from "../../helpers";

export default class GamesListPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            games: []
        };
    }

    componentDidMount() {
        if (this.props.user === undefined) {
            this.props.history.push('/');
        }
        this.loadGames();
    }

    loadGames = () => {
        Axios.get(process.env.REACT_APP_API_ENDPOINT + '/list', {
            responseType: 'json'
        }).then((response) => {
            console.log('Received games:');
            console.log(response.data);
            this.setState({games: response.data})
        }).catch((error) => {
            console.log(error)
        })
    };

    joinGame = (gameUuid) => {
        if (this.props.user === undefined) {
            console.error('Пользователь не создан');
            alert('Пользователь не создан');
            return;
        }
        console.log(`Joining to game ${gameUuid}\nAs user ${JSON.stringify(this.props.user)}....`);
        Axios.post(process.env.REACT_APP_API_ENDPOINT + '/start', {
            tag: 'StartGameRequestParams',
            user_data: this.props.user,
            game_uuid: gameUuid
        }, {
            responseType: 'json'
        }).then((response) => {
            const game = response.data;
            console.log(game);
            this.props.setCurrentGame(game);
            this.props.history.push(`/play_game`);
        }).catch(catchAxiosErrors);
    };

    render() {
        return (
            <div className={'GamesListPage'}>
                {/*<span>User: {JSON.stringify(this.props.user)}</span>*/}
                <br/>
                <Link to={'/create_game'}><button>Создать свою игру</button></Link>
                <GamesList games={this.state.games} joinGame={this.joinGame}/>
            </div>
        )
    }
}
