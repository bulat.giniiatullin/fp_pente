import React from "react";

import blackStone from '../blackStone.svg';
import whiteStone from '../whiteStone.svg';

export default class Cell extends React.Component {
    constructor(props) {
        super(props);
    }

    handleClick = (event) => {
        event.preventDefault();
        this.props.doMove(this.props.x, this.props.y);
    };

    render() {
        return (
            <div className={`cell ${this.props.borderless ? 'borderless' : ''}`}>
                <div className={'stone'}>
                    {this.props.stone === null ?
                    <button className={'stoneImg'} onClick={this.handleClick}/>
                    : this.props.stone === 'WhiteStone' ?
                    <img src={whiteStone} alt='whiteStone' className={'stoneImg'}/>
                    : <img src={blackStone} alt='blackStone' className={'stoneImg'}/>}
                </div>
            </div>
        )
    }
}
