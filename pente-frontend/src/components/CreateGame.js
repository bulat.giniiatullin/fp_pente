import React from "react";
import Axios from "axios";

import { catchAxiosErrors } from '../helpers';

import loader from '../BeanEater.svg';

export default class CreateGame extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            waiting: false
        };
    }

    componentDidMount() {
        if (this.props.user === undefined) {
            this.props.history.push('/');
        }
    }

    handleChange = (event) => {
        this.setState({name: event.target.value});
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const game = {
            user_info: this.props.user,
            game_name: this.state.name,
            tag: 'CreateGameRequestParams'
        };
        console.log(`Creating game: ${JSON.stringify(game)}`);
        Axios.post(process.env.REACT_APP_API_ENDPOINT + '/create', game, {
            responseType: 'json'
        }).then((response) => {
            const createdGame = response.data;
            console.log(`Game ${JSON.stringify(createdGame)} created, waiting for opponent`);
            this.waitingInterval = setInterval(
                () => this.waitForOpponent(createdGame),
                3000
            );
            this.setState({waiting: true});
        }).catch(catchAxiosErrors);
    };

    waitForOpponent = (game) => {
        Axios.get(process.env.REACT_APP_API_ENDPOINT + `/check/${game.gameUuid}`, {
            responseType: 'json'
        }).then((response) => {
            const newGame = response.data;
            console.log(`Got game ${JSON.stringify(newGame)}`);
            if (newGame.tag === 'StartedGame') {
                clearInterval(this.waitingInterval);
                this.props.setCurrentGame(newGame);
                this.props.history.push(`/play_game`);
            }
        }).catch(catchAxiosErrors);
    };

    render() {
        return (
            <div className={'formContainer'}>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Название игры:
                        <input type="text" value={this.state.name} onChange={this.handleChange} />
                    </label>
                    <br/>
                    <input type="submit" value="Создать" />
                </form>
                <div id={'overlay'} style={{display: this.state.waiting ? 'block' : 'none'}}/>
                <div id={'overlayMessage'} style={{display: this.state.waiting ? 'block' : 'none'}}>
                    <img src={loader} alt="loader" />
                    <div className={'text'}>Ждем оппонента...</div>
                </div>
            </div>
        )
    }
}
