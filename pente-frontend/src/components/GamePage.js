import React from "react";
import Cell from "./Cell";
import Axios from "axios";

import { catchAxiosErrors } from '../helpers';

import loader from "../BeanEater.svg";
import blackStone from '../blackStone.svg';
import whiteStone from '../whiteStone.svg';

export default class GamePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            game: undefined,
            waiting: false,
            gameEndMessage: undefined
        }
    }

    componentDidMount() {
        if (this.props.user === undefined) {
            this.props.history.push('/');
        }
        if (this.props.game === undefined) {
            this.props.history.push('/games');
        }
        const game = this.props.game;
        // Если мы играем за черных, то ждем хода первого игрока
        if (game.players[1].userUuid === this.props.user.userUuid) {
            this.waitingInterval = setInterval(
                () => this.waitForOpponent(game.gameUuid, game.step),
                2000
            );
            this.setState({waiting: true, game: game});
        }
        this.setState({game: game})
    }

    static compareIntersections = (a, b) => {
        const ax = a[0].x;
        const ay = a[0].y;
        const bx = b[0].x;
        const by = b[0].y;
        if (ax === bx) {
            // Если x`ы равны, то сраниваем y`и в прямом порядке
            return ay - by;
        } else {
            // Сравниваем по y`ам в обратном порядке
            return bx - ax;
        }
    };

    doMove = (x, y) => {
        Axios.post(process.env.REACT_APP_API_ENDPOINT + '/move', {
            tag: 'MoveRequestDto',
            moveRequestGameUuid: this.state.game.gameUuid,
            moveRequestCoordinate: {
                tag: 'Coordinate',
                x: x,
                y: y
            }
        }, {
            responseType: 'json'
        }).then((response) => {
            const newGame = response.data;
            console.log(newGame);
            if (this.checkGameEnd(newGame)) {
                return;
            }
            this.waitingInterval = setInterval(
                () => this.waitForOpponent(newGame.gameUuid, newGame.step),
                2000
            );
            this.setState({waiting: true, game: newGame});
        }).catch(catchAxiosErrors);
    };

    waitForOpponent = (gameUuid, step) => {
        Axios.get(process.env.REACT_APP_API_ENDPOINT + `/check/${gameUuid}`, {
            responseType: 'json'
        }).then((response) => {
            const newGame = response.data;
            console.log(newGame);
            if (this.checkGameEnd(newGame)) {
                return;
            }
            if (newGame.step > step) {
                clearInterval(this.waitingInterval);
                this.setState({waiting: false, game: newGame});
            }
        }).catch(catchAxiosErrors);
    };

    checkGameEnd = (game) => {
        const status = game.winnerStatus;
        if (status.tag === 'Standoff') {
            this.setState({gameEndMessage: 'Ничья ¯\\_(ツ)_/¯', waiting: true});
            return true;
        } else if (status.tag === 'Winner') {
            if (status.winner.userUuid === this.props.user.userUuid) {
                this.setState({gameEndMessage: 'Ты супер. Ты победил!', waiting: true, game: game});
                return true;
            } else {
                this.setState({gameEndMessage: 'Ты луууузер. атщислен ©', waiting: true, game: game});
                return true;
            }
        }
        return false;
    };

    render() {
        let rows = [];
        if (this.state.game) {
            let intersections = this.state.game.board.intersections;
            intersections.sort(GamePage.compareIntersections);
            rows = [];
            let cells = [];
            for (const intersection of intersections) {
                const x = intersection[0].x;
                const y = intersection[0].y;
                cells.push(<Cell x={x} y={y} stone={intersection[1]} doMove={this.doMove} borderless={x === 19 || y === 1} key={`${x}:${y}`}/>);
                if (y === 19) {
                    rows.push(<div className={'row'} key={x}>{cells}</div>);
                    cells = [];
                }
            }
        }
        return (
            <div>
                <div className={'GamePage'}>
                    <div id={'board'}>
                        <div className={'table'} id={'backgroundTable'}>
                            {rows}
                        </div>
                    </div>
                    {this.state.game !== undefined ?
                    <div id={'gameInfo'}>
                        <div>Ход: {this.state.game.step}</div>
                        <div>
                            <img className={'smallImg'} src={whiteStone} alt='whiteStone'/>: {this.state.game.players[0].username}</div>
                        <div>
                            <img className={'smallImg'} src={blackStone} alt='blackStone'/>: {this.state.game.players[1].username}</div>
                        <div>Взято камней: <br />
                            <img className={'smallImg'} src={whiteStone} alt='whiteStone'/> {this.state.game.removedStones[0]} : <img className={'smallImg'} src={blackStone} alt='blackStone'/> {this.state.game.removedStones[1]}</div>
                    </div>
                        : <div id={'gameInfo'}/>}
                </div>
                <div id={'overlay'} style={{display: this.state.waiting ? 'block' : 'none'}}/>
                {this.state.gameEndMessage === undefined ?
                <div id={'overlayMessage'} style={{display: this.state.waiting ? 'block' : 'none'}}>
                    <img src={loader} alt="loader" />
                    <div className={'text'}>Ждем хода оппонента...</div>
                </div>
                    :
                    <div id={'overlayMessage'} style={{display: this.state.waiting ? 'block' : 'none'}}>
                        <div className={'text'}>{this.state.gameEndMessage}</div>
                    </div>
                }
            </div>
        )
    }
}
