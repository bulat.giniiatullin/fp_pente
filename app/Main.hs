module Main where

import Control.Concurrent.STM
import Types
import Api (app)
import Network.Wai.Handler.Warp

main :: IO ()
main = do
  let initialState = State []
  stateVar <- atomically $ newTVar $ initialState
  run 8081 (app stateVar)
