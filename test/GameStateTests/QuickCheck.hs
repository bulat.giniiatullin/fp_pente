module GameStateTests.QuickCheck (tests) where

import Test.Tasty
import Test.Tasty.QuickCheck

import Types
import Logic.GameStateLogic
import Helpers
import QuickCheckHelpers

    
replaceGameInStateTestCase = testProperty "replaceGameInStateTestCase" $
    \(State initGames) oldGame newGame -> 
    let 
        gamesBefore = oldGame:initGames
    in
        case replaceGameInState oldGame newGame (State gamesBefore) of 
            State gamesAfter -> length gamesBefore === length gamesAfter

getGamesFromStateTestCase = testProperty "getGamesFromState" $
    \state@(State games) -> length games >= length (getGamesFromState state)
                                    
             
tests = testGroup "GameStateQuickCheck" [
    replaceGameInStateTestCase,
    getGamesFromStateTestCase
    ]
