module GameStateTests.SmallCheck (tests) where

import Test.Tasty
import Test.Tasty.SmallCheck

import Types
import Logic.GameStateLogic
import Helpers
import SmallCheckHelpers

    
replaceGameInStateTestCase = localOption (SmallCheckDepth 3) $ testProperty "replaceGameInStateTestCase" $
    \(State initGames) oldGame newGame -> 
    let 
        gamesBefore = oldGame:initGames
    in
        case replaceGameInState oldGame newGame (State gamesBefore) of 
            State gamesAfter -> length gamesBefore == length gamesAfter

getGamesFromStateTestCase = localOption (SmallCheckDepth 5) $ testProperty "getGamesFromState" $
    \state@(State games) -> length games >= length (getGamesFromState state)
                                    
             
tests = testGroup "GameStateQuickCheck" [
    replaceGameInStateTestCase,
    getGamesFromStateTestCase
    ]
