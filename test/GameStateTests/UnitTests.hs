module GameStateTests.UnitTests (tests) where

import Test.Tasty
import Test.Tasty.HUnit

import Types
import Logic.GameStateLogic
import Helpers


-- Test Cases
createBoardTestCase = testCase "createBoardTestCase" (assertBool "should return empty Board" $ isBoardEmpty createBoard)

createStartedGameTestCase = testCase "createStartedGameTestCase" $ do
    let startedGame = createStartedGame testWaitingGame testUser2
    case startedGame of
        (StartedGame uuid name (player1, player2) board step removedStones status) -> do
            assertEqual "Game UUID should be the same" (gameUuid testWaitingGame) uuid
            assertEqual "Game name should be the same" (gameName testWaitingGame) name
            assertEqual "First player should be get from waiting game" (firstPlayer testWaitingGame) player1
            assertEqual "Second user should be the given" testUser2 player2
            assertBool "Board should be empty" $ isBoardEmpty board
            assertEqual "Step should be equal to one" 1 step
            assertBool "Removed stones should be zero" (fst removedStones == 0 && snd removedStones == 0)
            assertEqual "Status should be in progress" GameInProgress status
        _ -> assertFailure "should return StartedGame"

findGameByUuidTestCase = testCase "findGameByUuidTestCase" $ do
    let state = Types.State [testWaitingGame, testStartedGame]
    assertEqual "Should return WaitingGame" (Just testWaitingGame) $ findGameByUuid (gameUuid testWaitingGame) state
    assertEqual "Should return StartedGame" (Just testStartedGame) $ findGameByUuid (gameUuid testStartedGame) state
    assertEqual "Should return Nothing if game not in state" Nothing $ findGameByUuid (gameUuid testWaitingGame2) state

replaceGameInStateTestCase = testCase "replaceGameInStateTestCase" $ do
    let state = Types.State [testWaitingGame, testStartedGame]
    assertEqual "Should replace WaitingGame" [testWaitingGame2, testStartedGame] $ getListFromState (replaceGameInState testWaitingGame testWaitingGame2 state)
    assertEqual "Should replace StartedGame" [testStartedGame2, testWaitingGame] $ getListFromState (replaceGameInState testStartedGame testStartedGame2 state)
    assertEqual "Should replace WaitingGame with StartedGame" [testStartedGame2, testStartedGame] $ getListFromState (replaceGameInState testWaitingGame testStartedGame2 state)

isWaitingGameTestCase = testCase "isWaitingGameTestCase" $ do
    assertBool "True if WaitingGame" $ isWaitingGame testWaitingGame
    assertBool "False if StartedGame" (not $ isWaitingGame testStartedGame)

getGamesFromStateTestCase = testCase "getGamesFromStateTestCase" $ do
    let state = Types.State [testWaitingGame, testStartedGame, testWaitingGame2, testStartedGame2]
    assertEqual "Should return only WaitingGames" [testWaitingGame, testWaitingGame2] $ getGamesFromState state
    let state = Types.State [testStartedGame, testStartedGame2]
    assertEqual "Should empty list if there are no WaitingGames" [] $ getGamesFromState state

tests = testGroup "GameStateUnitTests" [
    createBoardTestCase,
    createStartedGameTestCase,
    findGameByUuidTestCase,
    replaceGameInStateTestCase,
    isWaitingGameTestCase,
    getGamesFromStateTestCase
    ]
