module StepTests.SmallCheck (tests) where

import Test.Tasty
import Test.Tasty.SmallCheck

import Types
import Logic.StepLogic
import Logic.GameStateLogic
import Helpers
import SmallCheckHelpers


getPrevNumberTestCase = testProperty "getPrevNumberTestCase" $ 
    \n -> n /= 0 ==> abs n > abs (getPrevNumber n)
    
incrementStepTestCase = localOption (SmallCheckDepth 4) $ testProperty "incrementStep" $ 
    \gameBefore -> not (isWaitingGame gameBefore) ==> case incrementStep (Coordinate 5 5) gameBefore of
            Right gameAfter -> step gameBefore < step gameAfter
             
tests = testGroup "StepSmallCheck" [
    getPrevNumberTestCase,
    incrementStepTestCase
    ]
