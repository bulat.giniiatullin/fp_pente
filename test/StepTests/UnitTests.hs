module StepTests.UnitTests (tests)  where

import Test.Tasty
import Test.Tasty.HUnit
import Data.Map ((!?), (!))
import Control.Monad (replicateM)
import Data.Either (fromRight, isLeft)

import Types
import Helpers
import Logic.StepLogic

doMoveTestCase = testCase "doMoveTestCase" $ do
    assertBool "Should make step" . checkState [(10, 10, WhiteStone)] . fromRight testStartedGame $ doMove testStartedGame (Coordinate 10 10)
    assertBool "Tournament rule works" . isLeft $ doMove testStartedGame (Coordinate 10 15)
    let game = (gameWithState getState1) {step=7}
    assertBool "Should eat opponent's stones" . checkState getState1 . fromRight game $ doMove game (Coordinate 3 5)
    let game = (gameWithState getState1) {step=7, removedStones=(4, 8)}
    assertEqual "Should end game" (Winner testUser) . winnerStatus . fromRight game $ doMove game (Coordinate 3 5)

checkStepTestCase = testCase "checkStepTestCase" $ do
    assertEqual "Step on free intersection is ok" (Right testStartedGame) $ checkStep (Coordinate 10 10) testStartedGame
    let game = gameWithState getState1
    assertEqual "Step on already used intersection is prohibited" (Left "The intersection is used") $ checkStep (Coordinate 3 3) game
    assertEqual "Step out of board prohibited" (Left "This intersection not exists") $ checkStep (Coordinate 100 100) testStartedGame

checkIfRemovableAndRemoveTestCase = testCase "checkIfRemovableAndRemoveTestCase" $ do
    let game = gameWithState getState1
    assertBool "Vert" . checkState getState1 . fromRight game $ checkIfRemovableAndRemove (Coordinate 3 5) game
    let game = gameWithState getState1
    assertEqual "False" (Right game) $ checkIfRemovableAndRemove (Coordinate 4 5) game
    let game = gameWithState getState2
    assertBool "Diag" . checkState getState2 . fromRight game $ checkIfRemovableAndRemove (Coordinate 4 4) game
    let game = gameWithState getState3
    assertBool "Hor" . checkState getState3 . fromRight game $ checkIfRemovableAndRemove (Coordinate 4 3) game
    let game = gameWithState getState4
    assertEqual "Hor" (Right game) $ checkIfRemovableAndRemove (Coordinate 6 6) game
    let game = gameWithState getState5
    assertBool "Hor" . checkState getState5 . fromRight game $ checkIfRemovableAndRemove (Coordinate 4 4) game

listToPairTestCase = testCase "listToPairTestCase" $
    assertEqual "Should create pair from first two elements of list" (1, 2) $ listToPair $ cycle [1, 2, 3]

checkIfRemovableBetweenAndRemoveTestCase = testCase "checkIfRemovableBetweenAndRemoveTestCase" $ do
    let game = gameWithState getState1
    assertBool "Vert" $ checkState getState1 $ checkIfRemovableBetweenAndRemove (Coordinate 3 5) (0, -3) game
    let game = gameWithState getState1
    assertEqual "False" game $ checkIfRemovableBetweenAndRemove (Coordinate 4 5) (-3, -3) game
    let game = gameWithState getState2
    assertBool "Diag" $ checkState getState2 $ checkIfRemovableBetweenAndRemove (Coordinate 4 4) (-3, -3) game
    let game = gameWithState getState3
    assertBool "Hor" $ checkState getState3 $ checkIfRemovableBetweenAndRemove (Coordinate 4 3) (-3, 0) game

checkIfOppositeBetweenTestCase = testCase "checkIfOppositeBetweenTestCase" $ do
    let game = gameWithState getState1
    assertBool "Vert" $ checkIfOppositeBetween 0 (-2) (Coordinate 3 5) game
    let game = gameWithState getState1
    assertBool "False" $ not $ checkIfOppositeBetween (-2) (-2) (Coordinate 4 5) game
    let game = gameWithState getState2
    assertBool "Diag" $ checkIfOppositeBetween (-2) (-2) (Coordinate 4 4) game
    let game = gameWithState getState3
    assertBool "Hor" $ checkIfOppositeBetween (-2) 0 (Coordinate 4 3) game

removeOppositeBetweenTestCase = testCase "removeOppositeBetweenTestCase" $ do
    let game = gameWithState getState1
    assertBool "Vert" $ checkState getState1 $ removeOppositeBetween 0 (-2) (Coordinate 3 5) game
    let game = gameWithState getState2
    assertBool "Diag" $ checkState getState2 $ removeOppositeBetween (-2) (-2) (Coordinate 4 4) game
    let game = gameWithState getState3
    assertBool "Hor" $ checkState getState3 $ removeOppositeBetween (-2) 0 (Coordinate 4 3) game

incrementRemovedStonesTestCase = testCase "incrementRemovedStonesTestCase" $ do
    assertEqual "If Black's step increment removed white stones" (1,0) $ incrementRemovedStones $ testStartedGame {removedStones=(0,0), step=2}
    assertEqual "If Whites's step increment removed black stones" (0,1) $ incrementRemovedStones $ testStartedGame {removedStones=(0,0), step=1}

getPrevNumberTestCase = testCase "getPrevNumberTestCase" $ do
    assertEqual "If positive should return -1" (-4) $ getPrevNumber (-5)
    assertEqual "If negative should return +1" 4 $ getPrevNumber 5
    assertEqual "If zero should return zero" 0 $ getPrevNumber 0

checkEndTestCase = testCase "checkEndTestCase" $ do
    let coord = Coordinate 0 0
    let game = testStartedGame {removedStones=(6, 10)}
    let eitherGame = checkEnd coord game
    assertEqual "First player win if removed black stones become gte 10" (Right $ game {winnerStatus=(Winner testUser)}) eitherGame
    let game = testStartedGame {removedStones=(10, 8)}
    let eitherGame = checkEnd coord game
    assertEqual "Second player win if removed white stones become gte 10" (Right $ game {winnerStatus=(Winner testUser2)}) eitherGame
    let game = testStartedGame {removedStones=(6, 8), board=createFullBoard}
    let eitherGame = checkEnd coord game
    assertEqual "Standoff if board full in removed stomes less then 10" (Right $ game {winnerStatus=Standoff}) eitherGame
    assertEqual "if no game end return the same game" (Right testStartedGame) $ checkEnd coord testStartedGame

makeStepTestCase = testCase "makeStepTestCase" $ do
    let eitherGame = makeStep (Coordinate 10 10) $ testStartedGame {step=1}
    case eitherGame of
        Left _ -> assertFailure "should return right game"
        Right game -> do
            let boardIntersections = intersections . board $ game
            assertBool "All intersections except of step target should be empty"
                (all (\[x,y] -> boardIntersections !? (Coordinate x y) == Just Nothing || (x == 10 && y == 10)) (replicateM 2 [1..19])
                    && boardIntersections ! (Coordinate 10 10) == Just WhiteStone)

incrementStepTestCase = testCase "incrementStepTestCase" $
    assertEqual "Should increment step" (Right $ testStartedGame {step=2}) $ incrementStep (Coordinate 10 10) testStartedGame

currentColorTestCase = testCase "currentColorTestCase" $ do
    assertEqual "If step odd color should be White" WhiteStone $ currentColor (testStartedGame {step=1})
    assertEqual "If step even color should be Black" BlackStone $ currentColor (testStartedGame {step=2})

oppositeColorTestCase = testCase "oppositeColorTestCase" $ do
    assertEqual "If step odd color should be Black" BlackStone $ oppositeColor (testStartedGame {step=1})
    assertEqual "If step even color should be White" WhiteStone $ oppositeColor (testStartedGame {step=2})

checkTournamentRuleTestCase = testCase "checkTournamentRuleTestCase" $ do
    let eitherGame = checkTournamentRule (Coordinate 10 10) $ testStartedGame {step=1}
    assertEqual "If first step correct should return the same game" (Right $ testStartedGame {step=1}) eitherGame
    let eitherGame = checkTournamentRule (Coordinate 15 10) $ testStartedGame {step=1}
    assertEqual "If first step incorrect should return Left" (Left "On White's first move stone must be placed at the center of the board") eitherGame
    let eitherGame = checkTournamentRule (Coordinate 6 13) $ testStartedGame {step=3}
    assertEqual "If second step correct should return the same game" (Right $ testStartedGame {step=3}) eitherGame
    let eitherGame = checkTournamentRule (Coordinate 11 9) $ testStartedGame {step=3}
    assertEqual "Second first step incorrect should return Left" (Left "On White's second move stone must be placed at 3 intersections farther from the center") eitherGame
    let eitherGame = checkTournamentRule (Coordinate 11 9) $ testStartedGame {step=6}
    assertEqual "In any other step should return the same game" (Right $ testStartedGame {step=6}) eitherGame

tests = testGroup "StepUnitTests" [
    doMoveTestCase,
    checkStepTestCase,
    checkIfRemovableAndRemoveTestCase,
    listToPairTestCase,
    checkIfRemovableBetweenAndRemoveTestCase,
    checkIfOppositeBetweenTestCase,
    removeOppositeBetweenTestCase,
    incrementRemovedStonesTestCase,
    getPrevNumberTestCase,
    checkEndTestCase,
    makeStepTestCase,
    incrementStepTestCase,
    currentColorTestCase,
    oppositeColorTestCase,
    checkTournamentRuleTestCase
    ]
