module StepTests.QuickCheck (tests) where

import Test.Tasty
import Test.Tasty.QuickCheck

import Types
import Logic.StepLogic
import Logic.GameStateLogic
import Helpers
import QuickCheckHelpers


getPrevNumberTestCase = testProperty "getPrevNumberTestCase" $ 
    \n -> n /= 0 ==> abs n > abs (getPrevNumber n)
    
incrementStepTestCase = testProperty "incrementStep" $ 
    \gameBefore -> not (isWaitingGame gameBefore) ==> case incrementStep (Coordinate 5 5) gameBefore of
            Right gameAfter -> step gameBefore < step gameAfter
             
tests = testGroup "StepQuickCheck" [
    getPrevNumberTestCase,
    incrementStepTestCase
    ]
