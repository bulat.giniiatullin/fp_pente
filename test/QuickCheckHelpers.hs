module QuickCheckHelpers where

import Test.Tasty.QuickCheck
import Test.QuickCheck.Instances.UUID

import Types
import Helpers


instance Arbitrary User where
  arbitrary = User <$> arbitrary <*> arbitrary
    
instance Arbitrary Game where
  arbitrary = oneof [waitingGen, startedGen]
  
instance Arbitrary State where
  arbitrary = State <$> arbitrary
  
waitingGen :: Gen Game
waitingGen = WaitingGame <$> arbitrary <*> arbitrary <*> arbitrary
      
startedGen :: Gen Game
startedGen = StartedGame <$> arbitrary <*> arbitrary <*> arbitrary <*> pure createFullBoard <*> arbitrary <*> arbitrary <*> pure GameInProgress 
