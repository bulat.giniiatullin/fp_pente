module Helpers where

import Data.UUID
import Data.Map ((!?), (!), insert, empty, Map)
import Control.Monad (replicateM)

import Types
import Logic.GameStateLogic

isBoardEmpty :: Board -> Bool
isBoardEmpty (Board intersections) = all (\[x,y] -> intersections !? (Coordinate x y) == Just Nothing) (replicateM 2 [1..19])

uuidFromString :: String -> UUID
uuidFromString str = unwrapJust $ fromString str
    where unwrapJust (Just value) = value

testUser :: User
testUser = User "46ee3c01-26c0-4fa1-8906-451e42607b63" "testUser"

testUser2 :: User
testUser2 = User "7070cdce-d306-45ca-b7b1-945d2831e691" "testUser2"

testStartedGame :: Game
testStartedGame = StartedGame (uuidFromString "13fb8c5f-3c5b-4986-ace2-ee1c53331805") "startedGameName" (testUser, testUser2) createBoard 1 (0, 0) GameInProgress

testStartedGame2 :: Game
testStartedGame2 = StartedGame (uuidFromString "01f908ca-fa0f-435e-b306-1a3f66ee504f") "startedGameName2" (testUser2, testUser) createBoard 1 (0, 0) GameInProgress

testWaitingGame :: Game
testWaitingGame = WaitingGame (uuidFromString "e3adb402-af24-46e7-9a6d-82b8cbdd5db6") "gameName" testUser

testWaitingGame2 :: Game
testWaitingGame2 = WaitingGame (uuidFromString "bcfa66b5-8ff8-4308-aa50-1d63adfd07f4") "gameName2" testUser2

getListFromState :: Types.State -> [Game]
getListFromState (State games) = games

createFullBoard :: Board
createFullBoard = Board (helper Data.Map.empty boardSize boardSize)
    where helper board x y | x > 0 && y > 1 = helper (Data.Map.insert (Coordinate x y) (Just BlackStone) board) x (y - 1)
                           | x > 0 && y == 1 = helper (Data.Map.insert (Coordinate x y) (Just WhiteStone) board) (x - 1) boardSize
                           | otherwise = board

type TestState = [(Int, Int, Stone)]

insertManyStones :: TestState -> Map Coordinate (Maybe Stone)
insertManyStones = foldr (\(x, y, stone) m -> Data.Map.insert (Coordinate x y) (Just stone) m) (intersections createBoard)

gameWithState :: TestState -> Game
gameWithState state = testStartedGame {board=(Board $ insertManyStones state)}

getState1 :: TestState
getState1 = [(3, 3, BlackStone), (3, 4, BlackStone), (3, 2, WhiteStone)]

getState2 :: TestState
getState2 = [(2, 2, BlackStone), (3, 3, BlackStone), (1, 1, WhiteStone)]

getState3 :: TestState
getState3 = [(2, 3, BlackStone), (3, 3, BlackStone), (1, 3, WhiteStone)]

getState4 :: TestState
getState4 = [(2, 2, BlackStone), (3, 3, BlackStone), (4, 4, BlackStone), (5, 5, BlackStone), (1, 1, WhiteStone)]

getState5 :: TestState
getState5 = [(2, 2, BlackStone), (3, 3, BlackStone), (5, 5, BlackStone), (6, 6, BlackStone), (1, 1, WhiteStone), (7, 7, WhiteStone)]

checkState :: TestState -> Game -> Bool
checkState state (StartedGame {board=(Board intersections)}) = all checkIntersection state
    where
        checkIntersection (x, y, BlackStone) = intersections ! (Coordinate x y) == Nothing
        checkIntersection (x, y, WhiteStone) = intersections ! (Coordinate x y) == Just WhiteStone
