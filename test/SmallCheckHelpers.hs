{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}

module SmallCheckHelpers where

import Test.Tasty.SmallCheck
import Test.SmallCheck.Series
import Data.UUID

import Types
import Helpers


instance (Monad m) => Serial m User
    
instance (Monad m) => Serial m Game where
  series = startedGameSeries \/ waitingGameSeries
  
instance (Monad m) => Serial m State where
  series = cons1 State
  
waitingGameSeries :: (Monad m) => Series m Game
waitingGameSeries = decDepth $ WaitingGame
    <$> pure nil
    <~> series
    <~> series
    
startedGameSeries :: (Monad m) => Series m Game
startedGameSeries = decDepth $ StartedGame
    <$> pure nil
    <~> series
    <~> series
    <~> pure createFullBoard
    <~> series
    <~> series
    <~> pure GameInProgress
