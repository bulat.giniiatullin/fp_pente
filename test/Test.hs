import Test.Tasty

import GameStateTests.UnitTests
import StepTests.UnitTests

import GameStateTests.QuickCheck
import StepTests.QuickCheck

import GameStateTests.SmallCheck
import StepTests.SmallCheck


unitTests = testGroup "AllTests" [GameStateTests.UnitTests.tests, StepTests.UnitTests.tests]
quickCheck = testGroup "QuickCheck" [GameStateTests.QuickCheck.tests, StepTests.QuickCheck.tests]
smallCheck = testGroup "SmallCheck" [GameStateTests.SmallCheck.tests, StepTests.SmallCheck.tests]
allTests = testGroup "AllTests" [unitTests, quickCheck, smallCheck]

main :: IO ()
main = do
    defaultMain allTests
